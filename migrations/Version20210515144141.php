<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210515144141 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE viajares_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE viajes_viajares_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE viajero_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE viajeros_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE viajes_viajeros_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE viajero (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE viajeros (id INT NOT NULL, cedula VARCHAR(30) NOT NULL, nombre VARCHAR(255) NOT NULL, fecha_nac DATE NOT NULL, telefono VARCHAR(25) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE viajes_viajeros (id INT NOT NULL, id_viaje INT NOT NULL, id_viajero INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('DROP TABLE viajares');
        $this->addSql('DROP TABLE viajes_viajares');
        $this->addSql('ALTER TABLE viajes ALTER codigo TYPE VARCHAR(100)');
        $this->addSql('ALTER TABLE viajes ALTER destino TYPE VARCHAR(250)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE viajero_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE viajeros_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE viajes_viajeros_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE viajares_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE viajes_viajares_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE viajares (id INT NOT NULL, cedula VARCHAR(30) NOT NULL, nombre VARCHAR(255) NOT NULL, fecha_nac DATE DEFAULT NULL, telefono VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE viajes_viajares (id INT NOT NULL, id_viajare INT NOT NULL, id_viaje INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('DROP TABLE viajero');
        $this->addSql('DROP TABLE viajeros');
        $this->addSql('DROP TABLE viajes_viajeros');
        $this->addSql('ALTER TABLE viajes ALTER codigo TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE viajes ALTER destino TYPE VARCHAR(255)');
    }
}
