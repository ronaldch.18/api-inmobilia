<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210515225327 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE viajes_viajero_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE reservacion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE viajes_viajeros_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE reservacion (id INT NOT NULL, id_viaje INT NOT NULL, id_viajero INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE viajes_viajeros (id INT NOT NULL, id_viaje INT NOT NULL, id_viajero INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('DROP TABLE viajes_viajero');
        $this->addSql('ALTER TABLE viajeros ALTER cedula TYPE VARCHAR(30)');
        $this->addSql('ALTER TABLE viajes ALTER codigo TYPE VARCHAR(100)');
        $this->addSql('ALTER TABLE viajes ALTER destino TYPE VARCHAR(250)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE reservacion_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE viajes_viajeros_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE viajes_viajero_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE viajes_viajero (id INT NOT NULL, id_viaje INT NOT NULL, id_viajero INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('DROP TABLE reservacion');
        $this->addSql('DROP TABLE viajes_viajeros');
        $this->addSql('ALTER TABLE viajes ALTER codigo TYPE VARCHAR(30)');
        $this->addSql('ALTER TABLE viajes ALTER destino TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE viajeros ALTER cedula TYPE VARCHAR(25)');
    }
}
