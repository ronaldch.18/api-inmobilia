<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210515225559 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE reservaciones_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE reservaciones (id INT NOT NULL, id_viaje INT NOT NULL, id_viajero INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE viajes_viajeros (id INT NOT NULL, id_viaje INT NOT NULL, id_viajero INT NOT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE reservaciones_id_seq CASCADE');
        $this->addSql('DROP TABLE reservaciones');
        $this->addSql('DROP TABLE viajes_viajeros');
    }
}
