<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210516152013 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE reservacion_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE viajes_viajeros_id_seq CASCADE');
        $this->addSql('ALTER TABLE reservaciones ADD id_viajero_id INT NOT NULL');
        $this->addSql('ALTER TABLE reservaciones RENAME COLUMN id_viajero TO id_viaje_id');
        $this->addSql('ALTER TABLE reservaciones ADD CONSTRAINT FK_46408D55C43A0ADD FOREIGN KEY (id_viaje_id) REFERENCES viajes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE reservaciones ADD CONSTRAINT FK_46408D555F7E4F3E FOREIGN KEY (id_viajero_id) REFERENCES viajeros (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_46408D55C43A0ADD ON reservaciones (id_viaje_id)');
        $this->addSql('CREATE INDEX IDX_46408D555F7E4F3E ON reservaciones (id_viajero_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE reservacion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE viajes_viajeros_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('ALTER TABLE reservaciones DROP CONSTRAINT FK_46408D55C43A0ADD');
        $this->addSql('ALTER TABLE reservaciones DROP CONSTRAINT FK_46408D555F7E4F3E');
        $this->addSql('DROP INDEX IDX_46408D55C43A0ADD');
        $this->addSql('DROP INDEX IDX_46408D555F7E4F3E');
        $this->addSql('ALTER TABLE reservaciones ADD id_viajero INT NOT NULL');
        $this->addSql('ALTER TABLE reservaciones DROP id_viaje_id');
        $this->addSql('ALTER TABLE reservaciones DROP id_viajero_id');
    }
}
