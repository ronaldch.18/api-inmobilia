<?php

namespace App\Controller;

use App\Repository\ReservacionesRepository;
use App\Repository\ViajesRepository;
use App\Repository\ViajerosRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;


/**
* Class ReservacionesController
* @package App\Controller
*
* @Route(path="/api/")
*/

Class ReservacionesController{
	private $reservacionesRepository;
	private $viajesRepository;
	private $viajerosRepository;

	public function __construct(ReservacionesRepository $reservacionesRepository, ViajesRepository $viajesRepository,ViajerosRepository $viajerosRepository ){
		$this->reservacionesRepository = $reservacionesRepository;
		$this->viajesRepository        = $viajesRepository;
		$this->viajerosRepository      = $viajerosRepository;
	}

	/**
	*@Route("reservaciones/{id}", name="get_reservacion_by_id", methods={"GET"})
	*/

	public function fetch($id): JsonResponse{
		$reservacion = $this->reservacionesRepository->findOneby(['id' => $id]);

		if(!empty($reservacion)){
			//Se Obtienen los datos asociados al Viaje
			$data  =  $this->viajesRepository->findOneby(['id' => $reservacion->getIdViaje()->getId()]);
			$viaje = [ 'id'         => $data->getId(), 
					   'codigo'     => $data->getCodigo(), 
					   'num_plazas' => $data->getNumPlazas(), 
					   'destino'    => $data->getDestino(), 
					   'origen'     => $data->getOrigen(), 
					   'precio'     => $data->getPrecio() 
					];

			//Se Obtienen los datos asociados al Viajero
			$data =  $this->viajerosRepository->findOneby(['id' => $reservacion->getIdViajero()->getId()]);
			$viajero = ['id'         => $data->getId(),
						'cedula'     => $data->getCedula(),
						'nombre'     => $data->getNombre(),
						'fecha_nac'  => $data->getFechaNac(),
						'telefono'   => $data->getTelefono()
					];


			$data = [  'id' 		=> $reservacion->getId(),
					   'id_viaje'   => $viaje,
				       'id_viajero' => $viajero
					];
		}else{
			$data = [
				'status'	 => '01',
				'message'    => 'Reservacion no encontrada'
			];
		}

		return new JsonResponse($data, Response::HTTP_OK);
	}

	/**
	*@Route("reservaciones", name="get_all_reservaciones", methods={"GET"})
	*/

	public function fetchAll(): JsonResponse{
		$reservaciones = $this->reservacionesRepository->findAll();


		if(!empty($reservaciones)){
			foreach ($reservaciones as $reservacion) {		
				//Se Obtienen los datos asociados al Viaje
				$result  =  $this->viajesRepository->findOneby(['id' => $reservacion->getIdViaje()->getId()]);
				$viaje = [ 'id'         => $result->getId(), 
					   	   'codigo'     => $result->getCodigo(), 
					   	   'num_plazas' => $result->getNumPlazas(), 
					   	   'destino'    => $result->getDestino(), 
					       'origen'     => $result->getOrigen(), 
					  	   'precio'     => $result->getPrecio() 
						];

				//Se Obtienen los datos asociados al Viajero
				$result =  $this->viajerosRepository->findOneby(['id' =>$reservacion->getIdViajero()->getId()]);
				$viajero = ['id'        => $result->getId(),
							'cedula'    => $result->getCedula(),
							'nombre'    => $result->getNombre(),
							'fecha_nac' => $result->getFechaNac(),
							'telefono'  => $result->getTelefono()
						];
		

				$data [] = ['id' 		 => $reservacion->getId(),
					   		'id_viaje'   => $reservacion->getIdViaje()->getId(),
				       	 	'id_viajero' => $reservacion->getIdViajero()->getId()
						];

			}
			$data[] = ['status' => '00'];
		}else{
			$data = [
				'status'=> '01', 
				'message' => 'Reservacion no encontrada'
			];
		}
		

		return new JsonResponse($data, Response::HTTP_OK);
	}


	/**
	*@Route("reservaciones", name="add_reservaciones", methods={"POST"})
	*/

	public function add(Request $request): JsonResponse{

		$data = json_decode($request->getContent(),true);

		$id_viaje   = $data['id_viaje'];
		$id_viajero = $data['id_viajero'];

		if(empty($id_viaje) || empty($id_viajero)){
			throw new NotFoundHttpException('Parametros incompletos!');
		}

		$reservacion  = $this->reservacionesRepository->findOneBy(['id_viaje' => $id_viaje, 'id_viajero' => $id_viajero]);
		$viajero      = $this->viajerosRepository->findOneBy(['id' => $id_viajero]);
		$viaje        = $this->viajesRepository->findOneBy(['id' => $id_viaje]);

		if(empty($viajero) ||empty($viaje)){
			$response = ['status'  => '01',
						 'message' => 'Viaje o Viajero no encontrado'];

		}else if(empty($reservacion)){
			$reservacion = $this->reservacionesRepository->saveReservacion($viaje, $viajero);
			$response = ['status'  => '00', 
						 'message' => 'Reservacion Creada'];

		}else{
			$response = ['status'=> '01',
			 			 'message' => 'Reservacion existente'];
		}

		return new JsonResponse($response, Response::HTTP_OK);
	}


	/**
	*@Route("reservaciones/{id}", name="update_reservacion", methods={"PUT"})
	*/
	public function update($id, Request $request): JsonResponse{

		$reservacion = $this->reservacionesRepository->findOneby(['id' => $id]);

		if(!empty($reservacion)){
			$data = json_decode($request->getContent(),true);

			empty($data['id_viaje']) ? true : $reservacion->setIdViaje($data['id_viaje']);
			empty($data['id_viajero']) ? true : $reservacion->setIdViajero($data['id_viajero']);

			$updateReservacion = $this->reservacionesRepository->updateReservacion($reservacion);
			$response    = ($updateReservacion) ? ['status'=> '00', 'message'=> 'Reservacion Actualizado'] : ['status'=> '01', 'message'=> 'Actualizacion Fallida'];
		}else{
			$response = ['status'=> '01', 
						 'message' => 'Reservacion no encontrada'];
		}
		
		return new JsonResponse($response, Response::HTTP_OK);

		
	}


	/**
	*@Route("reservaciones/{id}", name="delete_reservacion", methods={"DELETE"})
	*/
	public function delete($id, Request $request): JsonResponse{

		$response = [];
		$reservacion  = $this->reservacionesRepository->findOneby(['id' => $id]);
		
		if(!empty($reservacion)){
			$this->reservacionesRepository->deleteReservacion($reservacion);
			$response = ['status'=> '00', 
						 'message' => 'Reservacion Eliminada'] ;

		}else{
			$response = ['status'=> '01', 
						 'message' => 'Reservacion no encontrada'];
		}
		

		return new JsonResponse($response, Response::HTTP_OK);
	}

}

?>