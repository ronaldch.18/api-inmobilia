<?php

namespace App\Controller;

use App\Repository\ViajerosRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use \DateTime;


/**
* Class ViajerosController
* @package App\Controller
*
* @Route(path="/api/")
*/

Class ViajerosController{
	private $viajerosRepository;

	public function __construct(ViajerosRepository $viajerosRepository){
		$this->viajerosRepository = $viajerosRepository;
	}

	/**
	*@Route("viajeros/{id}", name="get_viajero_by_id", methods={"GET"})
	*/

	public function fetch($id): JsonResponse{
		$viajero = $this->viajerosRepository->findOneby(['id' => $id]);

		if(!empty($viajero)){
			$data = [
				'id' 		 => $viajero->getId(),
				'cedula'     => $viajero->getCedula(),
				'nombre'    => $viajero->getNombre(),
				'fecha_nac'  => $viajero->getFechaNac(),
				'telefono'   => $viajero->getTelefono()

			];
		}else{
			$data = [
				'status'	 => '01',
				'message'    => 'Viajero no encontrado'
			];
		}

		return new JsonResponse($data, Response::HTTP_OK);
	}

	/**
	*@Route("viajeros", name="get_all_viajeros", methods={"GET"})
	*/

	public function fetchAll(): JsonResponse{
		$viajeros = $this->viajerosRepository->findAll();
		$data = ['status' => '00'];

		if(!empty($viajeros)){
			foreach ($viajeros as $viajero) {			
				$data [] = [
					'id' 		 => $viajero->getId(),
					'cedula'     => $viajero->getCedula(),
					'nombre' 	 => $viajero->getNombre(),
					'fecha_nac'  => $viajero->getFechaNac(),
					'telefono'   => $viajero->getTelefono()
				];
			}
		}else{
			$data = [
				'status'	 => '01',
				'message'    => 'Viajero no encontrado'
			];
		}

		return new JsonResponse($data, Response::HTTP_OK);
	}


	/**
	*@Route("viajeros", name="add_viajeros", methods={"POST"})
	*/

	public function add(Request $request): JsonResponse{

		$data = json_decode($request->getContent(),true);

		$cedula     = $data['cedula'];
		$nombre     = $data['nombre'];
		$fecha_nac  = $data['fecha_nac'];
		$telefono   = $data['telefono'];

		if(empty($cedula) || empty($nombre) || empty($fecha_nac) || empty($telefono)){
			throw new NotFoundHttpException('Parametros incompletos!');
		}

		$viajero  = $this->viajerosRepository->findOneby(['cedula' => $cedula]);

		if(empty($viajero)){
			$this->viajerosRepository->saveViajero($cedula, $nombre, $fecha_nac, $telefono);
			return new JsonResponse(['status'=> '00', 'message' => 'Viajero Creado'], Response::HTTP_CREATED);
		}else{
			return new JsonResponse(['status'=> '01', 'message' => 'Viajero ya existe'], Response::HTTP_OK);
		}
	}


	/**
	*@Route("viajeros/{id}", name="update_viajero", methods={"PUT"})
	*/
	public function update($id, Request $request): JsonResponse{

		$viajero = $this->viajerosRepository->findOneby(['id' => $id]);

		if(!empty($viajero)){
			$data = json_decode($request->getContent(),true);

			empty($data['cedula']) ? true : $viajero->setCedula($data['cedula']);
			empty($data['nombre']) ? true : $viajero->setNombre($data['nombre']);
			empty($data['fecha_nac']) ? true : $viajero->setFechaNac(new DateTime($data['fecha_nac']));
			empty($data['telefono']) ? true : $viajero->setTelefono($data['telefono']);

			$updateViajero = $this->viajerosRepository->updateViajero($viajero);
			$response    = ($updateViajero) ? ['status'=> 'Viajero Actualizado'] : ['status'=> 'Actualizacion Fallida'];
		}else{
			$response = ['status'=> 'Viajero no encontrado'];
		}


		return new JsonResponse($response, Response::HTTP_OK);
	}


	/**
	*@Route("viajeros/{id}", name="delete_viajero", methods={"DELETE"})
	*/
	public function delete($id, Request $request): JsonResponse{

		$response = [];
		$viajero  = $this->viajerosRepository->findOneby(['id' => $id]);
		
		if(!empty($viajero)){
			$this->viajerosRepository->deleteViajero($viajero);
			$response = ['status'=> 'Viajero Eliminado'] ;
		}else{
			$response = ['status'=> 'Viajero no encontrado'];
		}
		

		return new JsonResponse($response, Response::HTTP_OK);
	}

}

?>