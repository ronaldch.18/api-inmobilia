<?php

namespace App\Controller;

use App\Repository\ViajesRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;


/**
* Class ViajesController
* @package App\Controller
*
* @Route(path="/api/")
*/

Class ViajesController{
	private $viajesRepository;

	public function __construct(ViajesRepository $viajesRepository){
		$this->viajesRepository = $viajesRepository;
	}

	/**
	*@Route("viajes/{id}", name="get_viaje_by_id", methods={"GET"})
	*/

	public function fetch($id): JsonResponse{
		$viaje = $this->viajesRepository->findOneby(['id' => $id]);

		if(!empty($viaje)){
			$data = [
				'id' 		 => $viaje->getId(),
				'codigo'     => $viaje->getCodigo(),
				'num_plazas' => $viaje->getNumPlazas(),
				'destino'    => $viaje->getDestino(),
				'origen'     => $viaje->getOrigen(),
				'precio'	 => $viaje->getPrecio(),
				'status'	 => '00'

			];
		}else{
			$data = [
				'status'	 => '01',
				'message'    => 'Viaje no encontrado'
			];
		}

		return new JsonResponse($data, Response::HTTP_OK);
	}

	/**
	*@Route("viajes", name="get_all_viajes", methods={"GET"})
	*/

	public function fetchAll(): JsonResponse{
		$viajes = $this->viajesRepository->findAll();
		$data = ['status' => '00'];

		if(!empty($viajes)){
			foreach ($viajes as $viaje) {			
				$data [] = [
					'id' 		 => $viaje->getId(),
					'codigo'     => $viaje->getCodigo(),
					'num_plazas' => $viaje->getNumPlazas(),
					'destino'    => $viaje->getDestino(),
					'origen'     => $viaje->getOrigen(),
					'precio'	 => $viaje->getPrecio()
				];
			}
		}else{
			$data = [
				'status'	 => '01',
				'message'    => 'Viaje no encontrado'
			];
		}

		return new JsonResponse($data, Response::HTTP_OK);
	}


	/**
	*@Route("viajes", name="add_viajes", methods={"POST"})
	*/

	public function add(Request $request): JsonResponse{

		$data = json_decode($request->getContent(),true);

		$codigo     = $data['codigo'];
		$num_plazas = $data['num_plazas'];
		$destino    = $data['destino'];
		$origen     = $data['origen'];
		$precio     = $data['precio'];

		if(empty($codigo) || empty($num_plazas) || empty($destino) || empty($origen) || empty($precio)){
			throw new NotFoundHttpException('Parametros incompletos!');
		}

		$this->viajesRepository->saveViaje($codigo, $num_plazas, $destino, $origen, $precio);

		return new JsonResponse(['status' => '00', 'message' => 'Viaje Creado'], Response::HTTP_CREATED);
	}


	/**
	*@Route("viajes/{id}", name="update_viaje", methods={"PUT"})
	*/
	public function update($id, Request $request): JsonResponse{

		$viaje = $this->viajesRepository->findOneby(['id' => $id]);

		if(!empty($viaje)){
			$data = json_decode($request->getContent(),true);

			empty($data['codigo']) ? true : $viaje->setCodigo($data['codigo']);
			empty($data['num_plazas']) ? true : $viaje->setNumPlazas($data['num_plazas']);
			empty($data['destino']) ? true : $viaje->setDestino($data['destino']);
			empty($data['origen']) ? true : $viaje->setOrigen($data['origen']);
			empty($data['precio']) ? true : $viaje->setPrecio($data['precio']);

			$updateViaje = $this->viajesRepository->updateViaje($viaje);
			$response    = ($updateViaje) ? ['status' => '00', 'message' => 'Viaje Actualizado'] : ['status' => '01', 'message'=> 'Actualizacion Fallida'];
		}else{
			$response = ['status' => '01',
						 'message'=> 'Viaje no encontrado'
						];
		}


		return new JsonResponse($response, Response::HTTP_OK);
	}


	/**
	*@Route("viajes/{id}", name="delete_viaje", methods={"DELETE"})
	*/
	public function delete($id, Request $request): JsonResponse{

		$response = [];
		$viaje    = $this->viajesRepository->findOneby(['id' => $id]);
		
		if(!empty($viaje)){
			$this->viajesRepository->deleteViaje($viaje);
			$response = ['status' => '00', 
						 'message' => 'Viaje Eliminado'
						] ;
		}else{
			$response = ['status' => '01', 
						 'message'=> 'Viaje no encontrado'
						];
		}
		
		return new JsonResponse($response, Response::HTTP_OK);
	}

}

?>