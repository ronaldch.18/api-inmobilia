<?php

namespace App\Entity;

use App\Repository\ReservacionesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReservacionesRepository::class)
 */
class Reservaciones
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Viajes::class, inversedBy="id_reservacion")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_viaje;

    /**
     * @ORM\ManyToOne(targetEntity=Viajeros::class, inversedBy="id_reservacion")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_viajero;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdViaje(): ?Viajes
    {
        return $this->id_viaje;
    }

    public function setIdViaje(?Viajes $id_viaje): self
    {
        $this->id_viaje = $id_viaje;

        return $this;
    }

    public function getIdViajero(): ?Viajeros
    {
        return $this->id_viajero;
    }

    public function setIdViajero(?Viajeros $id_viajero): self
    {
        $this->id_viajero = $id_viajero;

        return $this;
    }

}
