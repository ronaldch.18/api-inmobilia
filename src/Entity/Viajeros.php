<?php

namespace App\Entity;

use App\Repository\ViajerosRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ViajerosRepository::class)
 */
class Viajeros
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $cedula;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="date")
     */
    private $fecha_nac;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $telefono;

    /**
     * @ORM\OneToMany(targetEntity=Reservaciones::class, mappedBy="id_viajero")
     */
    private $reservaciones;

    public function __construct()
    {
        $this->reservaciones = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCedula(): ?string
    {
        return $this->cedula;
    }

    public function setCedula(string $cedula): self
    {
        $this->cedula = $cedula;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getFechaNac(): ?\DateTimeInterface
    {
        return $this->fecha_nac;
    }

    public function setFechaNac(\DateTimeInterface $fecha_nac): self
    {
        $this->fecha_nac = $fecha_nac;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * @return Collection|Reservaciones[]
     */
    public function getReservacion(): Collection
    {
        return $this->reservaciones;
    }

    public function addReservacion(Reservaciones $idReservacion): self
    {
        if (!$this->reservaciones->contains($idReservacion)) {
            $this->reservaciones[] = $idReservacion;
            $idReservacion->setIdViajero($this);
        }

        return $this;
    }

    public function removeReservacion(Reservaciones $idReservacion): self
    {
        if ($this->reservaciones->removeElement($idReservacion)) {
            // set the owning side to null (unless already changed)
            if ($idReservacion->getIdViajero() === $this) {
                $idReservacion->setIdViajero(null);
            }
        }

        return $this;
    }

}
