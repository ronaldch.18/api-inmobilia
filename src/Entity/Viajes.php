<?php

namespace App\Entity;

use App\Repository\ViajesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ViajesRepository::class)
 */
class Viajes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $codigo;

    /**
     * @ORM\Column(type="integer")
     */
    private $num_plazas;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $destino;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $origen;

    /**
     * @ORM\Column(type="float")
     */
    private $precio;

    /**
     * @ORM\OneToMany(targetEntity=Reservaciones::class, mappedBy="id_viaje")
     */
    private $reservaciones;

    public function __construct()
    {
        $this->reservaciones = new ArrayCollection();
    }

  

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    public function setCodigo(string $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getNumPlazas(): ?int
    {
        return $this->num_plazas;
    }

    public function setNumPlazas(int $num_plazas): self
    {
        $this->num_plazas = $num_plazas;

        return $this;
    }

    public function getDestino(): ?string
    {
        return $this->destino;
    }

    public function setDestino(string $destino): self
    {
        $this->destino = $destino;

        return $this;
    }

    public function getOrigen(): ?string
    {
        return $this->origen;
    }

    public function setOrigen(string $origen): self
    {
        $this->origen = $origen;

        return $this;
    }

    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    public function setPrecio(float $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * @return Collection|Reservaciones[]
     */
    public function getReservacion(): Collection
    {
        return $this->reservaciones;
    }

    public function addReservacion(Reservaciones $idReservacion): self
    {
        if (!$this->reservaciones->contains($idReservacion)) {
            $this->reservaciones[] = $idReservacion;
            $idReservacion->setIdViaje($this);
        }

        return $this;
    }

    public function removeReservacion(Reservaciones $idReservacion): self
    {
        if ($this->reservaciones->removeElement($idReservacion)) {
            // set the owning side to null (unless already changed)
            if ($idReservacion->getIdViaje() === $this) {
                $idReservacion->setIdViaje(null);
            }
        }

        return $this;
    }

}
