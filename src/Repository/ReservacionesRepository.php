<?php

namespace App\Repository;

use App\Entity\Reservaciones;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Reservaciones|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reservaciones|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reservaciones[]    findAll()
 * @method Reservaciones[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReservacionesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager) 
    {
        parent::__construct($registry, Reservaciones::class);
        $this->manager = $manager;
    }

   public function saveReservacion($id_viaje, $id_viajero): Reservaciones{
        $reservacion = new Reservaciones();

        $reservacion
            ->setIdViaje($id_viaje)
            ->setIdViajero($id_viajero);

        $this->manager->persist($reservacion);
        $this->manager->flush();

        return $reservacion;

    }


    public function updateReservacion(Reservaciones $reservacion): Reservaciones{
        
        $this->manager->persist($reservacion);
        $this->manager->flush();

        return $reservacion;
    }

    public function deleteReservacion(Reservaciones $reservacion){
        
        $this->manager->remove($reservacion);
        $this->manager->flush();
    }
}
