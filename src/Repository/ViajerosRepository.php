<?php

namespace App\Repository;

use App\Entity\Viajeros;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use \DateTime;


/**
 * @method Viajeros|null find($id, $lockMode = null, $lockVersion = null)
 * @method Viajeros|null findOneBy(array $criteria, array $orderBy = null)
 * @method Viajeros[]    findAll()
 * @method Viajeros[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ViajerosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager) 
    {
        parent::__construct($registry, Viajeros::class);
        $this->manager = $manager;
    }

    public function saveViajero($cedula, $nombre, $fecha_nac, $telefono){
        $viajero = new Viajeros();

        $viajero
            ->setCedula($cedula)
            ->setNombre($nombre)
            ->setFechaNac(new DateTime($fecha_nac))
            ->setTelefono($telefono);

        $this->manager->persist($viajero);
        $this->manager->flush();

    }


    public function updateViajero(Viajeros $viajero): Viajeros{
        
        $this->manager->persist($viajero);
        $this->manager->flush();

        return $viajero;
    }

    public function deleteViajero(Viajeros $viajero){
        
        $this->manager->remove($viajero);
        $this->manager->flush();
    }
}
