<?php

namespace App\Repository;

use App\Entity\Viajes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Viajes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Viajes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Viajes[]    findAll()
 * @method Viajes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ViajesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Viajes::class);
        $this->manager = $manager;
    }

    public function saveViaje($codigo, $num_plazas, $destino, $origen, $precio){
        $viaje = new Viajes();

        $viaje
            ->setCodigo($codigo)
            ->setNumPlazas($num_plazas)
            ->setDestino($destino)
            ->setOrigen($origen)
            ->setPrecio($precio);

        $this->manager->persist($viaje);
        $this->manager->flush();

    }


    public function updateViaje(Viajes $viaje): Viajes{
        
        $this->manager->persist($viaje);
        $this->manager->flush();

        return $viaje;
    }

    public function deleteViaje(Viajes $viaje){
        
        $this->manager->remove($viaje);
        $this->manager->flush();
    }


}
